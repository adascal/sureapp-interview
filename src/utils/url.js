import withQuery from 'with-query';
import get from 'lodash/get';
import omit from 'lodash/omit';
import filter from 'lodash/filter';
import pickBy from 'lodash/pickBy';
import replace from 'lodash/replace';
import includes from 'lodash/includes';
import trimEnd from 'lodash/trimEnd';

const origin = typeof window === 'undefined' ? '' : window.location.origin;
const protocol = typeof window === 'undefined' ? 'https:' : window.location.protocol;

const appPublicUrl = process.env.PUBLIC_URL ? `${protocol}${process.env.PUBLIC_URL}` : origin;
const apiPublicUrl = process.env.REACT_APP_API_PUBLIC_URL || appPublicUrl;

/**
 * Takes in a url string and trims off leading and trailing slashes '/'
 * @param {String} url
 */
export const trimUrl = url => {
  if (typeof url !== 'string') {
    return null;
  }

  let cleanUrl = url;

  // remove leading forward slash
  if (cleanUrl.startsWith('/')) {
    cleanUrl = cleanUrl.slice(1);
  }

  // remove trailing forward slash
  if (cleanUrl.endsWith('/')) {
    cleanUrl = cleanUrl.substr(0, cleanUrl.length - 1);
  }

  return cleanUrl;
};

/**
 * Combine all parts into single url
 */
export const combineUrl = (...paths) => `/${trimUrl(filter(paths, path => !!path).join('/'))}`;

export const combineAppUrl = (...paths) => `${appPublicUrl}${combineUrl(...paths)}`;

export const combineApiUrl = (...paths) => `${apiPublicUrl}${combineUrl(...paths)}`;

/**
 * Takes an url and params and returns combined url
 * @param {String} url
 * @param {Object} params
 */
export const withParams = (url, params = {}) => {
  let rest = { ...params };

  const urlWithParams = `${url}`
    .replace(/:([\w\d.]+)(\?)?/gim, (match, p1, p2) => {
      const hasDotJson = includes(p1, '.json');
      const normalizedP1 = replace(p1, '.json', '');
      const param = get(rest, normalizedP1, p2 && '');

      rest = omit(rest, normalizedP1);

      return `${param}${hasDotJson ? '.json' : ''}`;
    })
    .replace('//', '/');

  return withQuery(trimEnd(urlWithParams, '/'), pickBy(rest, p => p !== null && p !== undefined));
};
