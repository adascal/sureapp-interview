import timeAgo from 'timeago.js';

const timer = timeAgo();

export const ago = time => timer.format(time * 1000);
