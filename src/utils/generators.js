import get from 'lodash/get';

export const generateActionType = base => {
  const generator = actionType => `${base}/${actionType}`;

  generator.forRequest = actionType => [
    `${base}/${actionType}_INIT`,
    `${base}/${actionType}_DONE`,
    `${base}/${actionType}_FAIL`,
  ];

  return generator;
};

export const generateReducer = (reducerName, initialState, actionHandlers) => (state = initialState, action) => {
  const handler = get(actionHandlers, action.type);

  return handler ? handler(state, action) : state;
};
