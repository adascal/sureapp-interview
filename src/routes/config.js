import dynamic from 'utils/dynamic';

import CommonLayout from 'components/layouts/CommonLayout';

import appRoutes from './app';

export default [
  {
    path: appRoutes.home,
    component: CommonLayout,
    routes: [
      {
        exact: true,
        path: appRoutes.home,
        component: dynamic(() => import('components/pages/HomePage')),
      },
      {
        exact: true,
        path: appRoutes.news,
        component: dynamic(() => import('components/pages/NewsPage')),
      },
    ],
  },
];
