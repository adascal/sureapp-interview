import isFunction from 'lodash/isFunction';

function isPromiseLike(val) {
  return val && isFunction(val.then);
}

const promiseMiddleware = ({ dispatch, getState }) => next => action => {
  if (isFunction(action)) {
    // eslint-disable-next-line no-param-reassign
    action = action(dispatch, getState);

    if (isPromiseLike(action)) {
      return action;
    }
  }

  if (action) {
    const { promise, types, ...rest } = action;

    if (!promise) {
      return next(action);
    }

    const [INIT, DONE, FAIL] = types;

    next({ ...rest, type: INIT });

    return promise
      .then(result => {
        next({ ...rest, payload: result, type: DONE });

        return result;
      })
      .catch(error => {
        next({ ...rest, payload: error, type: FAIL });

        return { error };
      });
  }

  return action;
};

export default promiseMiddleware;
