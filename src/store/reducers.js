import { combineReducers } from 'redux';

import apiReducer, { initialState as apiInitialState, reducerName as apiReducerName } from 'modules/api/reducer';
import appReducer, { initialState as appInitialState, reducerName as appReducerName } from 'modules/app/reducer';
import storiesReducer, {
  initialState as storiesInitialState,
  reducerName as storiesReducerName,
} from 'modules/stories/reducer';
import favoritesReducer, {
  initialState as favoritesInitialState,
  reducerName as favoritesReducerName,
} from 'modules/favorites/reducer';

export const initialState = {
  [apiReducerName]: {
    ...apiInitialState,
  },
  [appReducerName]: {
    ...appInitialState,
  },
  [storiesReducerName]: {
    ...storiesInitialState,
  },
  [favoritesReducerName]: {
    ...favoritesInitialState,
  },
};

export const persistedReducers = [appReducerName, favoritesReducerName];

const makeRootReducer = asyncReducers =>
  combineReducers({
    [apiReducerName]: apiReducer,
    [appReducerName]: appReducer,
    [storiesReducerName]: storiesReducer,
    [favoritesReducerName]: favoritesReducer,

    ...asyncReducers,
  });

export default makeRootReducer;
