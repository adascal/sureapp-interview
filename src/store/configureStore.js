import createStore from './createStore';

export const configureStore = preloadedState => {
  const store = createStore(preloadedState);

  store.asyncReducers = {};
  store.preloadedState = preloadedState;

  return store;
};

export default configureStore;
