import { applyMiddleware, compose, createStore } from 'redux';
import persistState from 'redux-localstorage';

import makeRootReducer, { initialState, persistedReducers } from './reducers';
import { promiseMiddleware } from './middlewares';

export default (preloadedState = initialState) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [promiseMiddleware];

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = [persistState(persistedReducers)];
  let composeEnhancers = compose;

  /* eslint-disable no-underscore-dangle */
  if (process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
  }
  /* eslint-enable no-underscore-dangle */

  enhancers.push(applyMiddleware(...middleware));

  // ======================================================
  // Store Instantiation
  // ======================================================
  const store = createStore(makeRootReducer(), preloadedState, composeEnhancers(...enhancers));

  return store;
};
