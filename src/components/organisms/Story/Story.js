import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { ago } from 'utils/time';

import Link from 'components/atoms/Link';

import { loadStory } from 'modules/stories/actions';
import { favoriteStory, unfavoriteStory } from 'modules/favorites/actions';

import { getStoryById } from 'modules/stories/selectors';
import { isFavoriteStory } from 'modules/favorites/selectors';

import StoryPlaceholder from 'components/molecules/StoryPlaceholder';

import styles from './Story.module.scss';

class Story extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    id: PropTypes.number.isRequired,
    rank: PropTypes.number,
    item: PropTypes.shape({
      by: PropTypes.string.isRequired,
      score: PropTypes.number,
      time: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      url: PropTypes.string,
    }),
    isFavorite: PropTypes.bool,

    loadStory: PropTypes.func.isRequired,
    favoriteStory: PropTypes.func.isRequired,
    unfavoriteStory: PropTypes.func.isRequired,
  };

  static defaultProps = {
    className: null,
    rank: null,
    item: null,
    isFavorite: false,
  };

  componentDidMount() {
    this.props.loadStory(this.props.id);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.id !== this.props.id) {
      this.props.loadStory(this.props.id);
    }
  }

  onFavoriteClick = () => {
    const { id, isFavorite } = this.props;

    if (isFavorite) {
      this.props.unfavoriteStory(id);
    } else {
      this.props.favoriteStory(id);
    }
  };

  render() {
    const { className, rank, item, isFavorite } = this.props;

    return (
      <div className={classNames(styles.wrapper, className)}>
        <StoryPlaceholder ready={!!item}>
          {item ? (
            <React.Fragment>
              <h2 className={styles.title}>
                {rank && `${rank}. `}
                {item.title}
                {item.url && <Link href={item.url} className={styles.url} icon="external-link-alt" isExternal />}
              </h2>
              <Link
                icon="bookmark"
                onClick={this.onFavoriteClick}
                className={classNames(styles.bookmark, {
                  [styles.isFavorite]: isFavorite,
                })}
              />
              <div className={styles.stat}>
                {item.score} points by @{item.by} {ago(item.time)} | <span>{item.descendants} comments</span>
              </div>
            </React.Fragment>
          ) : (
            'Loading...'
          )}
        </StoryPlaceholder>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  item: getStoryById(props.id)(state, props),
  isFavorite: isFavoriteStory(props.id)(state, props),
});

const mapDispatchToProps = {
  loadStory,
  favoriteStory,
  unfavoriteStory,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Story);
