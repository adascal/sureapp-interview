import React from 'react';
import classNames from 'classnames';

import appRoutes from 'routes/app';

import Logo from 'components/atoms/Logo';
import Link from 'components/atoms/Link';
import Icon from 'components/atoms/Icon';
import Button from 'components/atoms/Button';

import styles from './Header.module.scss';

const Header = () => (
  <div className={styles.wrapper}>
    <div className={styles.innerWrapper}>
      <div className={styles.logoWrapper}>
        <Link href={appRoutes.home}>
          <Logo className={styles.logo} />
        </Link>
      </div>

      <div className={styles.linksWrapper}>
        <Link href="#" className={styles.headerItem} color={Link.colors.white} icon="sort-down">
          Products
        </Link>
        <Link href="#" className={styles.headerItem} color={Link.colors.white}>
          Support
        </Link>
        <Link href="#" className={styles.headerItem} color={Link.colors.white}>
          Claims
        </Link>
      </div>
    </div>

    <div className={styles.innerWrapper}>
      <div className={styles.linksWrapper}>
        <Link href="#" className={styles.headerItem} color={Link.colors.white}>
          Download App
        </Link>
        <Link href="#" className={styles.headerItem} color={Link.colors.white}>
          Login
        </Link>
        <Button kind={Button.kinds.secondary} className={styles.headerItem}>
          Sign Up
        </Button>
        <Link href="#" className={classNames(styles.headerItem, styles.menuIcon)}>
          <Icon name="bars" />
        </Link>
      </div>
    </div>
  </div>
);

export default Header;
