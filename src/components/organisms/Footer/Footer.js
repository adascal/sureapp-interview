import React from 'react';

import appRoutes from 'routes/app';

import Logo from 'components/atoms/Logo';
import Link from 'components/atoms/Link';
import Icon from 'components/atoms/Icon';

import styles from './Footer.module.scss';

const Footer = () => (
  <div className={styles.wrapper}>
    <div className={styles.logoWrapper}>
      <Link href={appRoutes.home}>
        <Logo color={Logo.colors.blue} className={styles.logo} />
      </Link>
      <p className={styles.copyright}>© 2017-2018 Sure Inc. All rights reserved.</p>
    </div>
    <div className={styles.linksWrapper}>
      <Link href="#" className={styles.link}>
        Privacy Policy
      </Link>
      <Link href="#" className={styles.link}>
        Terms of Service
      </Link>
      <Link href="#" className={styles.link}>
        Licenses
      </Link>
      <Link href="#" className={styles.link}>
        Products
      </Link>
      <Link href="#" className={styles.link}>
        Help Center
      </Link>
      <Link href="#" className={styles.link}>
        Contact Us
      </Link>
    </div>

    <div className={styles.iconsWrapper}>
      <Link href="#" className={styles.link}>
        <Icon name={['fab', 'twitter']} className={styles.icon} />
      </Link>
      <Link href="#" className={styles.link}>
        <Icon name={['fab', 'facebook']} className={styles.icon} />
      </Link>
      <Link href="#" className={styles.link}>
        <Icon name={['fab', 'instagram']} className={styles.icon} />
      </Link>
    </div>
  </div>
);

export default Footer;
