import React from 'react';
import map from 'lodash/map';

import Button from 'components/atoms/Button';
import Image from 'components/atoms/Image';

import appRoutes from 'routes/app';

import example from 'assets/images/example.png';
import * as partnersImages from 'assets/images/partners';
import * as coveragesIcons from 'assets/icons/coverages';

import styles from './HomePage.module.scss';

const features = [
  {
    title: 'Lorem',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet.',
  },
  {
    title: 'Ipsum',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet. ',
  },
  {
    title: 'Dolor',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet.',
  },
];

const coverages = [
  {
    title: 'Pet Coverage',
    image: coveragesIcons.pet,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },
  {
    title: 'Travel Coverage',
    image: coveragesIcons.travel,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },
  {
    title: 'Property Coverage',
    image: coveragesIcons.property,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },
  {
    title: 'Catastrophic Event Coverage',
    image: coveragesIcons.event,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },
  {
    title: 'Home Coverage',
    image: coveragesIcons.home,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },
];

const HomePage = () => (
  <div className={styles.wrapper}>
    <div className={styles.sureYouWrapper}>
      <div className={styles.sureYouInnerWrapper}>
        <h1 className={styles.sureYou}>Sure + You.</h1>
        <p className={styles.sureYouText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididuntut labore et dolore
          magna aliqua. Ut enim ad minim veniam, quis nostrud.
        </p>
      </div>
      <div className={styles.getStartedWrapper}>
        <Button href={appRoutes.news} size={Button.sizes.big}>
          Get Started
        </Button>
        <Image src={example} className={styles.example} />
      </div>
    </div>

    <div className={styles.partnersWrapper}>
      {map(partnersImages, (partner, i) => (
        <div key={i} className={styles.partnerWrapper}>
          <Image src={partner} className={styles.partnerImage} />
        </div>
      ))}
    </div>

    <div className={styles.featuresWrapper}>
      <h2 className={styles.featuresIntro}>Lorem Ipsum Dolor Sit Amet Consectetur.</h2>
      <div>
        {map(features, (feature, i) => (
          <div key={i} className={styles.featureWrapper}>
            <h3 className={styles.featureTitle}>{feature.title}</h3>
            <p className={styles.featureDescription}>{feature.description}</p>
          </div>
        ))}
      </div>
    </div>

    <div className={styles.coveragesWrapper}>
      <div className={styles.coveragesInnerWrapper}>
        <h2 className={styles.coveragesTitle}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.
        </h2>
        <div className={styles.coveragesContainer}>
          {map(coverages, (coverage, i) => (
            <div key={i} className={styles.coverage}>
              <Image src={coverage.image} className={styles.coverageImage} />
              <div className={styles.coverageInfo}>
                <h3 className={styles.coverageTitle}>{coverage.title}</h3>
                <p className={styles.coverageDescription}>{coverage.description}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  </div>
);

export default HomePage;
