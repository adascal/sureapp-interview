import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import map from 'lodash/map';

import Lazy from 'components/atoms/Lazy';
import Link from 'components/atoms/Link';

import Story from 'components/organisms/Story';

import { loadTopStories } from 'modules/stories/actions';

import { getTopStories } from 'modules/stories/selectors';
import { getFavoriteStories } from 'modules/favorites/selectors';

import styles from './NewsPage.module.scss';

class NewsPage extends React.PureComponent {
  static propTypes = {
    topStories: PropTypes.arrayOf(PropTypes.number),
    favoriteStories: PropTypes.arrayOf(PropTypes.number),

    loadTopStories: PropTypes.func.isRequired,
  };

  static defaultProps = {
    topStories: [],
  };

  state = {
    showFavorites: false,
  };

  componentDidMount() {
    this.props.loadTopStories();
  }

  onShowTopStoriesClick = () => {
    this.setState(
      {
        showFavorites: false,
      },
      () => {
        Lazy.forceCheck();
      },
    );
  };

  onShowFavoriteStoriesClick = () => {
    this.setState(
      {
        showFavorites: true,
      },
      () => {
        Lazy.forceCheck();
      },
    );
  };

  render() {
    const { showFavorites } = this.state;
    const { topStories, favoriteStories } = this.props;

    const storiesToShow = showFavorites ? favoriteStories : topStories;

    return (
      <div className={styles.wrapper}>
        <Link
          className={classNames(styles.title, {
            [styles.isActive]: !showFavorites,
          })}
          onClick={this.onShowTopStoriesClick}
        >
          Top stories
        </Link>
        <Link
          className={classNames(styles.title, {
            [styles.isActive]: showFavorites,
          })}
          onClick={this.onShowFavoriteStoriesClick}
        >
          Favorite stories
        </Link>

        <div className={styles.storiesWrapper}>
          {storiesToShow.length ? (
            map(storiesToShow, (storyId, i) => (
              <Lazy key={storyId} height={110} resize>
                <Story id={storyId} rank={i + 1} className={styles.story} />
              </Lazy>
            ))
          ) : (
            <p className={styles.noStories}>Nothing here, yet...</p>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  topStories: getTopStories(state, props),
  favoriteStories: getFavoriteStories(state, props),
});

const mapDispatchToProps = {
  loadTopStories,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsPage);
