/* eslint-disable jsx-a11y/anchor-has-content */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import values from 'lodash/values';
import { Link as RouterLink } from 'react-router-dom';

import Icon from 'components/atoms/Icon';

import styles from './Link.module.scss';

const Link = ({ href, className, iconClassName, children, icon, color, isExternal, onClick, ...props }) => {
  const linkClassName = classNames(styles.link, styles[color], className);
  const Child = () => (
    <React.Fragment>
      {children}
      {icon && <Icon name={icon} className={classNames(styles.icon, iconClassName)} />}
    </React.Fragment>
  );

  if (!href && onClick) {
    return (
      <a {...props} onClick={onClick} className={linkClassName}>
        <Child />
      </a>
    );
  }

  if (isExternal) {
    return (
      <a {...props} href={href} onClick={onClick} className={linkClassName} target="_blank" rel="noopener noreferrer">
        <Child />
      </a>
    );
  }

  return (
    <RouterLink {...props} to={href} onClick={onClick} className={linkClassName}>
      <Child />
    </RouterLink>
  );
};

Link.colors = {
  white: 'white',
  blue: 'blue',
};

Link.propTypes = {
  href: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  icon: PropTypes.string,
  iconClassName: PropTypes.string,
  color: PropTypes.oneOf(values(Link.colors)),
  isExternal: PropTypes.bool,
  onClick: PropTypes.func,
};

Link.defaultProps = {
  href: null,
  className: null,
  icon: null,
  iconClassName: null,
  color: Link.colors.blue,
  isExternal: null,
  onClick: null,
};

export default Link;
