import React from 'react';
import PropTypes from 'prop-types';
import ReactPlaceholder from 'react-placeholder';

const Placeholder = props => <ReactPlaceholder {...props} />;

Placeholder.propTypes = {
  ready: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

Placeholder.defaultProps = {
  ready: false,
};

export default Placeholder;
