import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faSortDown, faBars, faExternalLinkAlt, faBookmark } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';

library.add(faSortDown, faBars, faExternalLinkAlt, faBookmark, faFacebook, faInstagram, faTwitter);

const Icon = ({ name, ...props }) => <FontAwesomeIcon {...props} icon={name} />;

Icon.propTypes = {
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  className: PropTypes.string,
};

Icon.defaultProps = {
  className: '',
};

export default Icon;
