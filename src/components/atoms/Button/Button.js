import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import values from 'lodash/values';

import Link from 'components/atoms/Link';

import styles from './Button.module.scss';

const Button = ({ className, href, kind, size, ...props }) => {
  const Component = href ? Link : 'button';

  return (
    <Component {...props} className={classNames(styles.button, styles[kind], styles[size], className)} href={href} />
  );
};

Button.sizes = {
  big: 'big',
};

Button.kinds = {
  primary: 'primary',
  secondary: 'secondary',
};

Button.propTypes = {
  href: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.oneOf(values(Button.sizes)),
  kind: PropTypes.oneOf(values(Button.kinds)),
};

Button.defaultProps = {
  href: null,
  className: null,
  size: null,
  kind: Button.kinds.primary,
};

export default Button;
