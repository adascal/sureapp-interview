import React from 'react';
import LazyLoad, { forceCheck } from 'react-lazyload';

const Lazy = props => <LazyLoad {...props} />;

Lazy.forceCheck = forceCheck;

export default Lazy;
