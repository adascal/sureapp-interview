import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import values from 'lodash/values';

import { ReactComponent as LogoSVG } from 'assets/icons/logo.svg';

import styles from './Logo.module.scss';

const Logo = ({ color, className, ...props }) => (
  <LogoSVG {...props} className={classNames(styles.logo, styles[color], className)} />
);

Logo.colors = {
  white: 'white',
  blue: 'blue',
};

Logo.propTypes = {
  color: PropTypes.oneOf(values(Logo.colors)),
  className: PropTypes.string,
};

Logo.defaultProps = {
  color: Logo.colors.white,
  className: null,
};

export default Logo;
