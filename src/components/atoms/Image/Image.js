/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Image.module.scss';

const Image = ({ src, className, ...props }) => (
  <img {...props} src={src} className={classNames(styles.image, className)} />
);

Image.propTypes = {
  src: PropTypes.string.isRequired,
  className: PropTypes.string,
};

Image.defaulProps = {
  className: null,
};

export default Image;
