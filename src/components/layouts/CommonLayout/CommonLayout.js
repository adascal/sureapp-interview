import React from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';

import Header from 'components/organisms/Header';
import Footer from 'components/organisms/Footer';

import styles from './CommonLayout.module.scss';

const CommonLayout = ({ route }) => (
  <div className={styles.wrapper}>
    <Header />
    <div className={styles.content}>{renderRoutes(route.routes)}</div>
    <Footer />
  </div>
);

CommonLayout.propTypes = {
  route: PropTypes.shape({
    routes: PropTypes.array,
  }).isRequired,
};

export default CommonLayout;
