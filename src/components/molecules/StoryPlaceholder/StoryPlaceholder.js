import React from 'react';

import Placeholder from 'components/atoms/Placeholder';

const StoryPlaceholder = props => <Placeholder {...props} type="text" rows={3} showLoadingAnimation />;

export default StoryPlaceholder;
