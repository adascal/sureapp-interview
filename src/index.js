import React from 'react';
import ReactDOM from 'react-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import { renderRoutes } from 'react-router-config';

import routes from './routes';
import configureStore from './store/configureStore';
import * as serviceWorker from './serviceWorker';

import 'styles/core.scss';

// ========================================================
// Browser History Setup
// ========================================================
const history = createBrowserHistory({
  basename: process.env.PUBLIC_URL,
});

// ========================================================
// Store Setup
// ========================================================
/* eslint-disable no-underscore-dangle */
const preloadedState = window.__PRELOADED_STATE__;
delete window.__PRELOADED_STATE__;
/* eslint-enable no-underscore-dangle */

const store = configureStore(preloadedState);

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root');

const App = (
  <Provider store={store}>
    <Router history={history}>{renderRoutes(routes)}</Router>
  </Provider>
);

ReactDOM.render(App, MOUNT_NODE);

serviceWorker.register();
