export { default as pet, ReactComponent as Pet } from './pet.svg';
export { default as travel, ReactComponent as Travel } from './travel.svg';
export { default as property, ReactComponent as Property } from './property.svg';
export { default as event, ReactComponent as Event } from './event.svg';
export { default as home, ReactComponent as Home } from './home.svg';
