import get from 'lodash/get';
import { createSelector } from 'reselect';

import { reducerName, initialState } from './reducer';

export const getState = state => get(state, reducerName, initialState);

export const getTopStories = createSelector(getState, state => get(state, 'top'));

export const getStories = createSelector(getState, state => get(state, 'items'));

export const getStoryById = id => createSelector(getStories, stories => get(stories, id));
