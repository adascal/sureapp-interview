import { generateReducer } from 'utils/generators';

import ACTION_HANDLERS from './actionHandlers';

export const initialState = {
  items: {},
  top: [],
};

export const reducerName = 'stories';

export default generateReducer(reducerName, initialState, ACTION_HANDLERS);
