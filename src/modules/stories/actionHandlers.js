import { LOAD_TOP_STORIES_DONE, LOAD_STORY_DONE } from './actionTypes';

const loadStoryDone = (state, { payload, meta: { id } }) => ({
  ...state,
  items: {
    ...state.items,
    [id]: payload,
  },
});

const loadTopStoriesDone = (state, { payload }) => ({
  ...state,
  top: payload,
});

const ACTION_HANDLERS = {
  [LOAD_STORY_DONE]: loadStoryDone,
  [LOAD_TOP_STORIES_DONE]: loadTopStoriesDone,
};

export default ACTION_HANDLERS;
