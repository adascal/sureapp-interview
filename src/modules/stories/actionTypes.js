import { generateActionType } from 'utils/generators';

const actionType = generateActionType('STORIES');

export const [LOAD_STORY_INIT, LOAD_STORY_DONE, LOAD_STORY_FAIL] = actionType.forRequest('LOAD_STORY');

export const [LOAD_TOP_STORIES_INIT, LOAD_TOP_STORIES_DONE, LOAD_TOP_STORIES_FAIL] = actionType.forRequest(
  'LOAD_TOP_STORIES',
);
