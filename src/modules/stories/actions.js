import apiRoutes from 'routes/api';
import { getRequest } from 'modules/api/actions';

import { withParams } from 'utils/url';

import {
  LOAD_STORY_INIT,
  LOAD_STORY_DONE,
  LOAD_STORY_FAIL,
  LOAD_TOP_STORIES_INIT,
  LOAD_TOP_STORIES_DONE,
  LOAD_TOP_STORIES_FAIL,
} from './actionTypes';

export const loadStory = id => dispatch =>
  dispatch({
    types: [LOAD_STORY_INIT, LOAD_STORY_DONE, LOAD_STORY_FAIL],
    promise: dispatch(getRequest(withParams(apiRoutes.stories.item, { id }))),
    meta: { id },
  });

export const loadTopStories = () => dispatch =>
  dispatch({
    types: [LOAD_TOP_STORIES_INIT, LOAD_TOP_STORIES_DONE, LOAD_TOP_STORIES_FAIL],
    promise: dispatch(getRequest(apiRoutes.stories.top)),
  });
