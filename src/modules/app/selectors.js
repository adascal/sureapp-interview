import get from 'lodash/get';

import { reducerName, initialState } from './reducer';

export const getState = state => get(state, reducerName, initialState);

export const getProps = (state, props) => props;
