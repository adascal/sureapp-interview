import get from 'lodash/get';
import { withParams, combineApiUrl } from 'utils/url';

import { REQUEST_INIT, REQUEST_DONE, REQUEST_FAIL, CLEAR_CACHE } from './actionTypes';

const requestInit = (url, options) => ({
  type: REQUEST_INIT,
  payload: url,
  meta: {
    ...options,
    url,
  },
});

const requestDone = (url, options, response) => ({
  type: REQUEST_DONE,
  payload: url,
  meta: {
    ...options,
    url,
    response,
  },
});

const requestFail = (url, options, error) => ({
  type: REQUEST_FAIL,
  payload: url,
  meta: {
    ...options,
    url,
    error,
  },
});

const request = (url, options) => dispatch => {
  const requestUrl = combineApiUrl(url);

  dispatch(requestInit(url, options));

  const promise = new Promise(async (resolve, reject) => {
    try {
      const response = await fetch(requestUrl, options);
      const json = (await response.json().catch(() => {})) || {};

      if (!response.ok) {
        throw json;
      }

      dispatch(requestDone(url, options, { json }));

      resolve(json);
    } catch (ex) {
      const error = ex.message || ex.Message || ex;

      dispatch(requestFail(url, options, error));

      reject(error);
    }
  });

  return promise;
};

const getDefaultHeaders = () => {
  const headers = {
    'Content-Type': 'application/json',
  };

  return headers;
};

export const getRequest = (url, params, options) => (dispatch, getState) =>
  dispatch(
    request(withParams(url, params), {
      ...options,
      method: 'GET',
      headers: {
        ...getDefaultHeaders(getState),
        ...get(options, 'headers', {}),
      },
    }),
  );

export const postRequest = (url, body, params, options) => (dispatch, getState) =>
  dispatch(
    request(withParams(url, params), {
      ...options,
      method: 'POST',
      headers: {
        ...getDefaultHeaders(getState),
        ...get(options, 'headers', {}),
      },
      body: JSON.stringify(body),
    }),
  );

export const putRequest = (url, body, params, options) => (dispatch, getState) =>
  dispatch(
    request(withParams(url, params), {
      ...options,
      method: 'PUT',
      headers: {
        ...getDefaultHeaders(getState),
        ...get(options, 'headers', {}),
      },
      body: JSON.stringify(body),
    }),
  );

export const deleteRequest = (url, params, options) => (dispatch, getState) =>
  dispatch(
    request(withParams(url, params), {
      ...options,
      method: 'DELETE',
      headers: {
        ...getDefaultHeaders(getState),
        ...get(options, 'headers', {}),
      },
    }),
  );

export const clearCache = () => dispatch =>
  dispatch({
    type: CLEAR_CACHE,
  });
