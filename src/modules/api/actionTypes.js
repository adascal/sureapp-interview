import { generateActionType } from 'utils/generators';

const actionType = generateActionType('API');

export const [REQUEST_INIT, REQUEST_DONE, REQUEST_FAIL] = actionType.forRequest('REQUEST');

export const CLEAR_CACHE = actionType('CLEAR_CACHE');
