import get from 'lodash/get';
import { createSelector } from 'reselect';

import { reducerName, initialState } from './reducer';

export const getState = state => get(state, reducerName, initialState);

export const getStateByHash = hash => createSelector(getState, state => get(state, hash));

export const getResponseByHash = hash => createSelector(getStateByHash(hash), state => get(state, 'response', {}));
