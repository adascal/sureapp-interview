import { generateReducer } from 'utils/generators';

import ACTION_HANDLERS from './actionHandlers';

export const initialState = {};

export const reducerName = 'api';

export default generateReducer(reducerName, initialState, ACTION_HANDLERS);
