import { FAVORITE_STORY, UNFAVORITE_STORY } from './actionTypes';

export const favoriteStory = id => dispatch =>
  dispatch({
    type: [FAVORITE_STORY],
    payload: id,
  });

export const unfavoriteStory = id => dispatch =>
  dispatch({
    type: [UNFAVORITE_STORY],
    payload: id,
  });
