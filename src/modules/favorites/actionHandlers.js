import uniq from 'lodash/uniq';
import pull from 'lodash/pull';

import { FAVORITE_STORY, UNFAVORITE_STORY } from './actionTypes';

const favoriteStory = (state, { payload }) => ({
  ...state,
  stories: uniq([...state.stories, payload]),
});

const unfavoriteStory = (state, { payload }) => ({
  ...state,
  stories: pull(state.stories, payload),
});

const ACTION_HANDLERS = {
  [FAVORITE_STORY]: favoriteStory,
  [UNFAVORITE_STORY]: unfavoriteStory,
};

export default ACTION_HANDLERS;
