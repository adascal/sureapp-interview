import { generateActionType } from 'utils/generators';

const actionType = generateActionType('FAVORITES');

export const FAVORITE_STORY = actionType('FAVORITE_STORY');

export const UNFAVORITE_STORY = actionType('UNFAVORITE_STORY');
