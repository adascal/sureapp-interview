import { generateReducer } from 'utils/generators';

import ACTION_HANDLERS from './actionHandlers';

export const initialState = {
  stories: [],
};

export const reducerName = 'favorites';

export default generateReducer(reducerName, initialState, ACTION_HANDLERS);
