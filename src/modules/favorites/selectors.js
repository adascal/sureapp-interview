import get from 'lodash/get';
import includes from 'lodash/includes';
import { createSelector } from 'reselect';

import { reducerName, initialState } from './reducer';

export const getState = state => get(state, reducerName, initialState);

export const getFavoriteStories = createSelector(getState, state => get(state, 'stories'));

export const isFavoriteStory = id => createSelector(getFavoriteStories, stories => includes(stories, id));
